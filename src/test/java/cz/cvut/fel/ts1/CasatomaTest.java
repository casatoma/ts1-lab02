package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CasatomaTest {

    @Test
    public void factorial_nIs5_return120(){
        Casatoma casatoma = new Casatoma();
        int actual = casatoma.factorial(5);
        Assertions.assertEquals(120,actual);
    }

    @Test
    public void factorialZero(){
        Casatoma casatoma = new Casatoma();
        int actual = casatoma.factorial(0);
        Assertions.assertEquals(1,actual);
    }

    @Test
    public void factorialTwo(){
        Casatoma casatoma = new Casatoma();
        int actual = casatoma.factorial(2);
        Assertions.assertEquals(2,actual);
    }

}
