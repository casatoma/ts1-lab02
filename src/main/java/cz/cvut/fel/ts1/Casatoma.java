package cz.cvut.fel.ts1;

public class Casatoma {

    public int factorial (int n){

        int result = n;
        for (int i = 0; i < n; i++){
            result = result * (n-i);
        }
        return result;

    }

}
